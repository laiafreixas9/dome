import cv2
import glob
import logging
import traceback
import time

from Camera import Camera, EmulatedCamera, DOMECamera
from Communications import Communications
from Processing import Processing, SwarmBehaviour
from Utils import Source, draw_circles
import arena

from Utils import DEBUG

CALIBRATE = False


class CameraController():
    def __init__(self, source: Source, ip, port):
        self.__comms = Communications(ip, port)
        self.__source = source
        calibration_file = "matrix_parameters.txt"
        central_pixel_file = "central_pixel_file_square_.txt"
        self.__cam = Camera(calibration_file, central_pixel_file)
        self.__processing = Processing(
            self.__cam.size(), "square.png", source == Source.HARDWARE)

        if self.__source == Source.FILE:
            self.__cam = EmulatedCamera(
                calibration_file, central_pixel_file, "img/volvox_bright_field")
        elif self.__source == Source.HARDWARE:
            self.__cam = DOMECamera(calibration_file, central_pixel_file)
        elif self.__source == Source.EMULATED:
            self.__cam = arena.Arena(calibration_file, central_pixel_file)

    def next_frame(self, projection_list):
        if self.__source == Source.EMULATED:
            return self.__cam.crop_frame(
                self.__cam.next_frame(projection_list))
        else:
            return self.__cam.crop_frame(self.__cam.next_frame())

    def convert_to_projector_coords(self, agent_list):

        # send agent coordinates to projector
        # empty list to store projection pixels
        projection_list = []
        for points in agent_list:
            coordinate = (points[0], points[1])
            transformedRadius = self.__cam.radiusTransform(
                points[2], points[3])

            transformedCoordinate = self.__cam.coordinateTransform(coordinate)

            # TODO : Transform direction to coordinate system
            transformed_direction = points[-1]
            projection_list.append([int(transformedCoordinate[0]), int(
                transformedCoordinate[1]), int(transformedRadius[0]), int(transformedRadius[1]), transformed_direction])
            # transmit projection list to projector and wait for response to
            # verify message recieved
        return projection_list

    def loop(self):
        agent_list = []
        projection_list = []

        coords = [[0, 0, 10, 10, None], [500, 500, 10, 10, None]]
        converted_coords = self.convert_to_projector_coords(coords)
        coords_msg = {
            "COORD": [
                converted_coords[0][0],
                converted_coords[0][1],
                converted_coords[1][0],
                converted_coords[1][1]]}
        self.__comms.transmit(coords_msg)
        self.__comms.recieve()
        should_send = True
        should_record = False
        counter = 0
        while True:
            counter += 1
            try:
                start = time.time()
                img = self.next_frame(agent_list)
                end = time.time()
                logging.debug(f"Frame took {end-start} secs")
                agent_list = []
                if not CALIBRATE:
                    agent_list = self.__processing.process_frame(img)

                else:
                    agent_list = [[200, 200, 10, 10, None],
                                  [400, 200, 10, 10, None],
                                  [200, 600, 10, 10, None]]
                    cv2.rectangle(
                        img, (100, 100), (500, 500), (150, 150, 0), 10)
                    draw_circles(img, agent_list)

                if should_record:
                    cv2.imwrite(f"img_{counter}.png", img)
                projection_list = self.convert_to_projector_coords(agent_list)

                if should_send:
                    self.__comms.transmit(projection_list)
                else:
                    self.__comms.transmit([])
                self.__comms.recieve()

                if True:
                    cv2.imshow("Image", img)
                    key = cv2.waitKey(1) & 0xFF
                    if key == ord("q"):
                        break
                    elif key == ord("w"):
                        self.__processing.set_behaviour(
                            SwarmBehaviour.LIGHT_OFF_ALL)
                    elif key == ord("l"):
                        self.__processing.set_behaviour(
                            SwarmBehaviour.STOP_ALL)
                    elif key == ord("r"):
                        should_record = not should_record
                    elif key == ord("p"):
                        self.__processing.set_behaviour(SwarmBehaviour.PATTERN)
                    elif key == ord("t"):
                        self.__processing.set_behaviour(SwarmBehaviour.TRAIL)

            except StopIteration:
                logging.info("End of image stream")
                break
            except BaseException as e:
                logging.error(f"Exception thrown! {e}")
                traceback.print_exc()
                break
        # End of processing
        self.__processing.end()
        # End of camera stream
        self.__comms.transmit("NONE")
        acceptmsg = self.__comms.recieve()
        # terminate socket connection where no messages are being
        # recieved
        if not acceptmsg or "NONE" in acceptmsg:
            logging.info("Connection finished")
        return agent_list
