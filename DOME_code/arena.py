import logging
import math
import random
import numpy as np
import enum
import cv2

from agent import Agent
from geometry import Rectangle, Point
from Camera import Camera
import Utils


class Pattern(enum.Enum):
    SQUARE = 1
    CIRCLE = 2
    DYNAMIC = 3


class Arena(Camera):
    def __init__(self, file, pixel_file, num_agents=10):
        super().__init__(file, pixel_file)
        self.__width, self.__height = self.size()
        self.__rectangle = Rectangle(0, 0, self.__width, self.__height)
        self.__agent_list = []
        self.initialise_agents(num_agents)
        self.position_to_circle = None

    def initialise_agents(self, num_agents: int, seed=42):
        random.seed(seed)
        for _ in range(num_agents):
            self.__agent_list.append(
                Agent(
                    self.__rectangle,
                    speed=10 * random.random(),
                    theta=random.uniform(
                        0,
                        2 * math.pi),
                    pos=self.__rectangle.random_point(seed),
                    frames_on=random.randint(
                        10,
                        20),
                    frames_off=random.randint(
                        3,
                        5)))

    def is_position_illuminated(self, agent: Agent, frame: np.array):
        position = agent.position()
        (px, py) = int(position.x()), int(position.y())
        if 0 <= py < frame.shape[0] and 0 <= px < frame.shape[1]:
            colour = frame[py, px]
            if Utils.DEBUG:
                if (colour == Utils.PROJECTION_COLOUR).all():
                    Utils.draw_circle(frame, (px, py), 5, (0, 0, 255), 2)
                else:
                    Utils.draw_circle(frame, (px, py), 5, (0, 255, 255), 2)
                Utils.write_text(
                    frame, f"{agent.speed()*1000:.2f}", (px, py), (255, 255, 255))

            if (colour == Utils.PROJECTION_COLOUR).all():
                # Find which circle is there
                closest_circle = None
                min_distance = None
                for point in self.position_to_circle.keys():
                    if min_distance is None or position.distance(
                            point) < min_distance:
                        min_distance = position.distance(point)
                        closest_circle = point
                if closest_circle is not None:
                    return True, self.position_to_circle[closest_circle]
                else:
                    return True, None
        return False, None

    def update(self, illumination):
        for agent in self.__agent_list:
            agent.update(
                self.is_position_illuminated(
                    agent,
                    illumination))

    def draw_current_projection_state(self, list):
        frame = np.zeros((self.__width, self.__height, 3), np.uint8)
        projector_background_colour = (50,) * 3
        frame[:] = projector_background_colour

        Utils.draw_circles(frame, list, Utils.PROJECTION_COLOUR, -1)
        self.position_to_circle = {}
        for circle in list:
            self.position_to_circle[Point(circle[0], circle[1])] = circle[-1]
        return frame

    def next_frame(self, projection_list):
        projector_frame = self.draw_current_projection_state(projection_list)
        self.update(projector_frame)
        if Utils.DEBUG:
            cv2.imshow("emulated projector", projector_frame)
        new_frame = np.zeros((self.__width, self.__height, 3), np.uint8)
        background_colour = (180,) * 3
        new_frame[:] = background_colour
        overlayed_img = new_frame.copy()
        for agent in self.__agent_list:
            pos = agent.position()
            edge_colour = random.randint(0, 100)
            colour = random.randint(75, 150)
            # Filled circle - with transparency
            cv2.circle(overlayed_img, (int(pos.x()), int(pos.y())), agent.radius(),
                       (colour, colour, colour), -1)

            cv2.circle(new_frame, (int(pos.x()), int(pos.y())), agent.radius() + 1,
                       (edge_colour, edge_colour, edge_colour), 1)
        new_frame = cv2.addWeighted(overlayed_img, 0.4, new_frame, 0.6, 0)

        # Add Gaussian noise
        #noise = np.zeros((self.__width, self.__height, 3), np.uint8)
        empty = np.zeros((self.__width, self.__height), np.uint8)
        imgray = cv2.randu(empty, 0, 25)
        noise = cv2.merge((imgray, imgray, imgray))
        return cv2.add(noise, new_frame)

    def crop_frame(self, frame):
        return frame
