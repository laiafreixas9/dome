import logging
import cv2
import numpy as np
import copy
import random
import math

from detection import Detection, TrackedDetection
import Utils


class Volvox():
    def __init__(self, csv_file, output_to_file):
        self.output_csv = csv_file
        self.__output_to_file = output_to_file

    def write_to_file(self, agent_list):
        """
        agent_list is a dictionary {id:Detection}
        Converts the dictionary agent_list to readable format
        and appends it to the output csv file
        If the dictionary is empty, it doesn't write anything
        """
        if agent_list:
            logging.debug(f"saving info from {len(agent_list)} agents")
            writable_list = {}
            for agent_id, detection in agent_list.items():
                writable_list[agent_id] = detection.to_list(
                    include_history=True)
            Utils.write_to_file(writable_list, self.output_csv)

    def find_agents(self, img):
        """
        Identify agent detections in a camera image
        Returns a list of all the detections in that image
        For each detection, it returns a Detection object
        x,y : center point
        w,h : width and height (currently the same value)
        """
        detected_agents = []
        grey = img[:, :, 2]
        #grey = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        grey = cv2.GaussianBlur(grey, (5, 5), 3)
        circles = cv2.HoughCircles(grey, cv2.HOUGH_GRADIENT_ALT, 2, 20,
                                   param1=100, param2=0.1, minRadius=0, maxRadius=30)

        if circles is not None:
            circles = np.uint16(np.around(circles))
            for (x, y, radius) in circles[:, 0]:
                detected_agents.append(Detection(x, y, radius, radius))
        return np.array(detected_agents)

    def match_detections(self, past_detections,
                         current_detections, time_difference_ms, global_counter):
        """
        Returns a dictionary of {id} : TrackedDetection with the matched agents
        past_detections is the dictionary of TrackedDetection of the last frames
        Tracked detection includes the velocity information for each agent
        current_detections is a list of the Detection this frame
        """

        matched_detections = {}
        discarded_detections = {}
        for id, d in past_detections.items():
            # becomes increasingly negative the more frames are dropped
            d.update_deactivity()
            # set current activity to deactive for all agents
            # but it will become 1 if matched in the next section
            d.reset_deactivity()
            # Check if we have seen it in the last 10 frames
            if d.is_active(10):
                # Only carry the active detections to prevent an evergrowing
                # list
                matched_detections[id] = d
            else:
                # Store the detection before discarding it
                discarded_detections[id] = d

        # Save all discarded to file
        self.write_to_file(discarded_detections)
        for current_detection in current_detections:
            match_score_threshold = 40

            # Try to match with any of the past detections
            for id, past_detection in matched_detections.items():
                score = past_detection.matching_score(current_detection)
                if score < match_score_threshold:
                    matched_detection_id = id
                    match_score_threshold = score

            # If the agent falls outside of a given confidence inteval, assume new agent
            # And add a new entry to the dictionary
            if match_score_threshold > 35:
                # select new id
                new_id = 0
                if len(past_detections) > 0:
                    new_id = max(past_detections.keys()) + 1
                matched_detections[new_id] = TrackedDetection(
                    current_detection, self.__output_to_file)
            else:
                # update the tracked detection
                matched_detections[matched_detection_id].update(
                    current_detection, time_difference_ms, global_counter)

        # carry over propogation number
        return matched_detections

    # parameters for duration and relaxation period of light flashing

    def update_global_light_params(self):
        light_duration_init = 1
        light_relaxation_init = 10
        return (light_duration_init, light_relaxation_init)
