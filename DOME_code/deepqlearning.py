
from collections import deque
import numpy as np
import random
import logging
import learning
from learning import Action, Behaviour
import datetime
import tensorflow as tf

# Following from
# https://towardsdatascience.com/deep-q-learning-tutorial-mindqn-2a4c855abffc
# Use 2 networks with same architecture but different weights
# Every  100 steps, copy the weights from the main network to the target network
# Also using this
# https://github.com/mswang12/minDQN/blob/main/minDQN.py
# Also check this
# https://adventuresinmachinelearning.com/reinforcement-learning-tensorflow/


class VolvoxLearningModel():
    # Deep Q learning

    def __init__(self):

        self.__observation_space_n = 4  # velocity, acceleration, frames on/off, status
        self.__action_space_n = 2  # on/off
        # Two NN
        self.__main_nn = learning.create_network(
            (self.__observation_space_n,), self.__action_space_n)
        self.__target_nn = learning.create_network(
            (self.__observation_space_n,), self.__action_space_n)
        # Every N steps, the weights from the main network are copied to the
        # target network
        self.__episode = 0

        self.__gamma = 0.3  # discount factor
        self.__alpha = 0.2  # learning rate
        self.__max_epsilon = 1
        self.__min_epsilon = 0.01
        self.__epsilon = self.__max_epsilon
        self.__decay = 0.001

        self.__last_action = None  # will be of type Action
        self.__last_state = None  # will be an index
        self.__behaviour = Behaviour.STOP
        self.__last_action_list = np.array([])
        self.__memory = deque()

        # Saving
        now = datetime.datetime.now()

        self.filename = 'output-' + str(now.minute) + ":" + str(now.second)
        self.filename_npy = self.filename + ".npy"
        self.filename_csv = self.filename + ".csv"
        self.filename_actions = "actions" + \
            str(now.minute) + ":" + str(now.second) + ".npy"

    def train(self):

        if len(self.__memory) < 1000:
            return
        batch_size = 128
        mini_batch = random.sample(self.__memory, batch_size)
        current_states = np.array([transition[0] for transition in mini_batch])
        predicted_actions_main = self.__main_nn.predict(current_states)
        new_current_states = np.array(
            [transition[3] for transition in mini_batch])
        predicted_actions_target = self.__target_nn.predict(new_current_states)

        X = []
        Y = []
        for index, (observation, action, reward,
                    new_observation) in enumerate(mini_batch):
            max_future_q = reward + self.__gamma * \
                np.max(predicted_actions_target[index])

            current_qs = predicted_actions_main[index]
            current_qs[action] = (
                1 - self.__alpha) * current_qs[action] + self.__alpha * max_future_q

            X.append(observation)
            Y.append(current_qs)

        self.__main_nn.fit(
            np.array(X),
            np.array(Y),
            batch_size=32,
            epochs=100,
            verbose=0,
            shuffle=True)

        now = datetime.datetime.now()
        with open(self.filename_csv, 'a') as f:
            f.write(f"{now.hour}:{now.minute}: {now.second}")
            f.write(",")
            f.write(str(float(self.__main_nn.metrics[0].result())))
            f.write("\n")

    def step(self, history, frames_off, frames_on, light_status):
        self.__episode += 1

        observation = [0, 0, frames_on - frames_off, light_status]
        if len(history) > 0:
            state_information = history[-1]
            observation[0] = state_information.velocity
            observation[1] = state_information.acceleration
        observation = np.array(observation)

        next_action = self.choose_action(observation)
        self.__last_action = next_action
        self.__last_state = observation
        current_reward = 5 if abs(observation[0]) < 0.05 else -1
        # Only keep the last 1000 actions
        self.__last_action_list = np.append(
            self.__last_action_list, next_action.value)
        self.__last_action_list = self.__last_action_list[-1000:]
        np.save(self.filename_actions, self.__last_action_list)

        self.__memory.append(
            [self.__last_state, self.__last_action.value, current_reward, observation])
        if self.__episode % 4 == 0:
            self.train()

        if self.__episode % 100 == 0:
            self.__target_nn.set_weights(self.__main_nn.get_weights())

        self.__epsilon = self.__min_epsilon + \
            (self.__max_epsilon - self.__min_epsilon) * \
            np.exp(-self.__decay * self.__episode)

        return self.__last_action

    def choose_action(self, state):
        # Exploration
        if random.uniform(0, 1) < self.__epsilon:
            return Action(random.randint(0, self.__action_space_n - 1))
        # Exploitation
        else:
            state_reshaped = state.reshape([1, state.shape[0]])
            predictions = self.__main_nn.predict(state_reshaped)
            return Action(np.argmax(predictions))

    def q_table(self):
        # Readable Q-table
        return []
