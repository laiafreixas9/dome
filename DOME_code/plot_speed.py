import csv
import glob
import datetime as dt
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
matplotlib.use("Agg")


def get_speed_xy(filename):
    x = []
    y = []
    with open(filename, newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        for row in reader:
            try:
                data_time = dt.datetime.strptime(row[0], "%H:%M:%S-%f")
                data_value = float(row[1])
                x.append(data_time)
                y.append(data_value)

            except BaseException as e:
                print(f"could not split row {row} - {e}")
                return None
    if len(x) < 10:
        return None
    return (x, y)


def plot_values(list_of_xy):
    fig, ax = plt.subplots()

    for (x, y) in list_of_xy:
        plt.scatter(x, y)


if __name__ == "__main__":
    xy_array = []
    for filename in glob.glob('*speed*.csv'):
        values = get_speed_xy(filename)
        if values is not None:
            xy_array.append(values)
    plot_values(xy_array)
    print(f"Plotting {len(xy_array)} cells")
    plt.savefig("plot_speeds.png")
