# This code is provided to facilitate calibration of camera to projector space in the DOME system
# and must be run in parallel with "projector_calibration.py"

# authors = Ana Rubio Denniss <am.rubiodenniss@bristol.ac.uk>, Thomas E. Gorochowski & Sabine Hauert
# affiliation = University of Bristol

# This work is licensed under a Creative Commons Attribution 4.0
# International License.

##########################################################################

import picamera
from picamera.array import PiRGBArray
from picamera import PiCamera
import RPi.GPIO as GPIO
import numpy as np
import time
import cv2
import csv
import statistics
import math
import socket
import json


##########################################################################

# SPECIFY CAMERA RESOLUTION AND IMPORT CROPPING PARAMETERS

resolution = (1920, 1088)


def center_point():
    with open("CentralPixel_file_.txt") as f:
        csvreader = csv.reader(f, delimiter=',', quotechar=None)
        for row in csvreader:
            center_row = int(row[0])
            center_column = int(row[1])
            center_row_length = int(row[2])
            center_column_length = int(row[3])
    center_point = (
        center_row,
        center_column,
        center_row_length,
        center_column_length)
    return center_point

##########################################################################

 # FUNCTION TO RECIEVE DATA FROM OTHER RASPBERRY PI NODE


def recieve():
    data = clientsocket.recv(1024)
    # provide an exception for when no data is received to avoid json expected
    # value error
    if not data:
        byteDecode = "NONE"
    # otherwise load seralised data using json
    else:
        byteDecode = json.loads(data)
    return(byteDecode)

##########################################################################

 # PERFORM A QUADRANT SEARCH TO ROUGHLY CALIBRATE PROJECTOR SPACE TO CAMERA
 # SPACE


def calibration_localisation(center_point=center_point):
    # specify frame crop parameters
    center_row = (center_point[0])
    center_column = (center_point[1])
    center_row_length = (center_point[2])
    center_column_length = (center_point[3])
    # initialize the camera and specify settings
    camera = PiCamera()
    camera.resolution = (resolution)
    camera.hflip = True
    camera.iso = 200
    camera.shutter_speed = 100000
    camera.exposure_mode = "spotlight"
    rawCapture = PiRGBArray(camera, size=resolution)
    # allow the camera to warmup
    time.sleep(0.5)
    for frame in camera.capture_continuous(
            rawCapture, format="bgr", use_video_port=True):
        image = frame.array
        image = image[center_column -
                      (center_column_length):center_column +
                      (center_column_length), center_row -
                      (center_row_length):center_row +
                      (center_row_length)]
        imgray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        smoothed = cv2.blur(imgray, (150, 150))
        ret, thresh = cv2.threshold(smoothed, 20, 255, 0)
        dilated = cv2.dilate(thresh, np.ones((7, 7), np.uint8), iterations=3)
        rawCapture.truncate(0)
        break
    # cv2.imshow("FRAME",image)
    # cv2.imshow("THRESHOLD",dilated)
    camera.close()

    # check middle pixel for illumination
    center_pixel_row = int(round(thresh.shape[1] / 2))
    center_pixel_column = int(round(thresh.shape[0] / 2))
    if (dilated[center_pixel_column][center_pixel_row] == 255):
        detected = 'DETECTED'
    else:
        detected = 'NOTDETECTED'
    k = cv2.waitKey(33)
    return detected

##########################################################################

 # IMAGE GRID PROJECTED BY PROJECTION MODULE AND EXTRACT GRID POINT COORDINATES


def grid_project(projector_centre_height, projector_centre_width,
                 grid_size, center_point=center_point):
    cv2.destroyAllWindows()
    # specify frame crop parameters
    center_row = (center_point[0])
    center_column = (center_point[1])
    center_row_length = (center_point[2])
    center_column_length = (center_point[3])
    # initialize the camera and specify settings
    camera = PiCamera()
    camera.resolution = (resolution)
    camera.hflip = True
    camera.shutter_speed = 100000
    camera.iso = 200
    camera.exposure_mode = "spotlight"
    rawCapture = PiRGBArray(camera, size=(resolution))
    counter = 0
    time.sleep(2)
    # allow some (here 10) frames to be cycled through before final image
    # obtained to ensure a high quality image
    for frame in camera.capture_continuous(
            rawCapture, format="bgr", use_video_port=True):
        image = frame.array
        image = image[center_column -
                      (center_column_length):center_column +
                      (center_column_length), center_row -
                      (center_row_length):center_row +
                      (center_row_length)]
        imgray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        smoothed = cv2.blur(imgray, (10, 10))
        ret, thresh = cv2.threshold(smoothed, 10, 255, 0)
        dilated = cv2.dilate(thresh, np.ones((5, 5), np.uint8), iterations=3)
        cv2.imshow("Grid", smoothed)
        cv2.imshow("Grid_thresh", thresh)
        rawCapture.truncate(0)
        counter += 1
        if (counter == 10):
            break
    camera.close()
    k = cv2.waitKey(33)

    # find bright spots in frame
    img, contours, hierarchy = cv2.findContours(
        dilated, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    cv2.drawContours(image, contours, -1, (40, 80, 0), 2)
    # specify region in which grid points should appear to avoid any potential
    # edge distortions or intereference
    shape = image.shape
    box = np.array([[50, 50], [50, shape[1] - 50], [shape[0] -
                   50, shape[1] - 50], [shape[0] - 50, 50]], dtype=np.int32)
    contours_list = []
    for c in contours:
        contours_list.append(c)
    cv2.imwrite("contourGrid.png", image)
    central_grids = []
    blank = np.zeros((image.shape[0], image.shape[1], 3), dtype=np.uint8)
    # check if points are within the specifiec region
    for c in contoursTrue:
        M = cv2.moments(c)
        cX = int(round(M["m10"] / M["m00"]))
        cY = int(round(M["m01"] / M["m00"]))
        point_check = cv2.pointPolygonTest(box, (cX, cY), False)
        if point_check == (1.0):
            central_grids.append([cX, cY])
        else:
            continue
        # draw the contour and center of the shape on the image
        cv2.drawContours(image, [c], -1, (0, 255, 0), 2)
        cv2.circle(blank, (cX, cY), 2, (0, 0, 255), -1)
        cv2.drawContours(blank, [box], -1, (255, 255, 0), 2)
    # check if all four points have been found, if not then terminate
    if len(central_grids) != (4):
        print("Grid not detected, ", len(central_grids), " points found")
        exit()
    # write grid points to file
    camera_grid_number = 0
    with open('cameraGrid.txt', 'w') as f:
        for points in central_grids:
            camera_grid_number = camera_grid_number + 1
            f.write("Grid: %s \n" % camera_grid_number)
            f.write("%s" % points[0])
            f.write(", %s\n" % points[1])
    cv2.imwrite("TrueContours.png", blank)
    projectorGrid = [[projector_centre_height - grid_size, projector_centre_width - grid_size], [projector_centre_height - grid_size, projector_centre_width + grid_size],
                     [projector_centre_height + grid_size, projector_centre_width - grid_size], [projector_centre_height + grid_size, projector_centre_width + grid_size]]
    projector_grid_number = 0
    with open('projectorGrid.txt', 'w') as f:
        for points in projector_grid:
            projector_grid_number = projector_grid_number + 1
            f.write("Grid: %s \n" % projector_grid_number)
            f.write("%s" % points[1])
            f.write(", %s\n" % points[0])

    print("HEIGHT", projector_centre_height)
    print("WIDTH", projector_centre_width)
    return


##########################################################################

 # FUNCTION TO READ PREVIOUSLY WRITTEN GRID POINT FILES

def file_reader(file):
    array = []
    with open(file) as f:
        csvreader = csv.reader(f, delimiter=',', quotechar=None)
        for row in csvreader:
            if row[0][0] == 'G':
                pass
            else:
                array.append([np.float32(row[0]), np.float32(row[1])])
    return array


##########################################################################

 # SORT ARRAY TO ASSURE THAT CONTOURS RETURNED BY FINDCONTOURS ARE WRITTEN
 # FROM TOP LEFT TO BOTTOM RIGHT

def sort_array(arr):
    arr = sorted(arr, key=lambda x: x[0])
    l = np.array_split(arr, 2)
    arr = sorted(l[0], key=lambda x: x[1]) + sorted(l[1], key=lambda x: x[1])
    return arr

##########################################################################

 # EXTRACT MATRIX TRANSFORMATION PARAMETERS FROM THE TWO SETS OF GRID
 # POINTS TO CALIBRATE PROJECTOR & CAMERA SPACE


def matrix_transformation(camera_array, projector_array):

    camera_array = sort_array(camera_array)
    projector_array = sort_array(projector_array)
    camera_array = np.array(camera_array)
    projector_array = np.array(projector_array)
    # use minAreaRect  to find center (x,y), (width, height), and angle of
    # rotation
    rect_proj = cv2.minAreaRect(projector_array)
    rect_cam = cv2.minAreaRect(camera_array)
    #reshape and transpose
    camera_array = np.reshape(camera_array, (4, 2)).T
    projector_array = np.reshape(projector_array, (4, 2)).T
    # define transformation parameters
    dx_cam = rect_cam[0][0]
    dy_cam = rect_cam[0][1]
    # theta parameter is the difference in rotation between arrays
    theta_cam = rect_cam[2]
    theta_proj = rect_proj[2]
    # check theta size and hence redefine with respect to rotation
    if (-45 < theta_cam <= 0):
        theta = theta_cam
    elif (-90 < theta_cam < -45):
        theta = theta_cam - theta_proj
    theta = math.radians(theta)
    stretch_x = rect_proj[1][0] / rect_cam[1][0]
    stretch_y = rect_proj[1][1] / rect_cam[1][1]
    dx_proj = rect_proj[0][0]
    dy_proj = rect_proj[0][1]
    # write transformation parameters to file
    with open('matrix_parameters.txt', 'w') as f:
        f.write("%s" % dx_cam)
        f.write(", %s" % dy_cam)
        f.write(", %s" % theta)
        f.write(", %s" % stretch_x)
        f.write(", %s" % stretch_y)
        f.write(", %s" % dx_proj)
        f.write(", %s" % dy_proj)
    return


center_point = center_point()

##########################################################################

# ESTABLISH SERVER SOCKET FOR PROJECTOR MODULE TO CONNECT TO AND RUN
# CALIBRATION ALGORITHM

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serversocket:
    serversocket.bind(('', 65455))
    serversocket.listen()
    clientsocket, addr = serversocket.accept()
    with clientsocket:
        while True:
            acceptmsg = recieve()
            print(acceptmsg)
            if "PROJECTING" in acceptmsg:
                sendmsg = calibration_localisation()
            elif "width_coordinate" in acceptmsg:
                height_coordinate = acceptmsg.get("height_coordinate")
                width_coordinate = acceptmsg.get("width_coordinate")
                grid_size = acceptmsg.get("grid_size")
                print(height_coordinate, width_coordinate, grid_size)
                sendmsg = "COORDINATESRECIEVED"
            elif "GRID" in acceptmsg:
                print(acceptmsg)
                gridProject(height_coordinate, width_coordinate, grid_size)
                projector_array = file_reader('projectorGrid.txt')
                camera_array = file_reader("cameraGrid.txt")
                matrix_transformation(camera_array, projector_array)
                sendmsg = "NONE"
            clientsocket.send(bytes(sendmsg, "utf-8"))
            # terminate socket connection where no messages are being recieved
            if not acceptmsg or "NONE" in acceptmsg:
                break

print("CALIBRATION COMPLETE")
