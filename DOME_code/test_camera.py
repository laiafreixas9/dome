import cv2
import glob
import logging
import traceback
import time

from Camera import Camera, EmulatedCamera, DOMECamera
from Communications import Communications
from Processing import Processing, SwarmBehaviour
from Utils import Source, draw_circles
import arena

from Utils import DEBUG
import Utils

if Utils.is_on_raspberry():
    from picamera.array import PiRGBArray
    from picamera import PiCamera, PiCameraError


with PiCamera() as cam:
    cam.resolution = (1920, 1088)
    print(cam.sensor_mode)
    print(cam.framerate)

with PiCamera() as cam:
    cam.resolution = (650, 656)
    print(cam.sensor_mode)
    print(cam.framerate)

logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.StreamHandler(),
        logging.FileHandler("test_zoom.log")
    ]
)


calibration_file = "matrix_parameters.txt"
central_pixel_file = "central_pixel_file_square_.txt"
cam = DOMECamera(calibration_file, central_pixel_file)
x_values = [440]
y_values = [700]
sizesx = []
sizesy = [325 / 1088.0, 328 / 1920.0]
img = cam.next_frame()
logging.info(f"image has shape {img.shape}")

cv2.imwrite(f"img_zoom_original.png", img)
for x in x_values:
    for y in y_values:
        for s in sizesy:
            for sy in sizesy:
                zoom = (x / 1088.0, y / 1920.0, s, sy)
                cam.set_zoom(zoom)
                img = cam.next_frame()
                cv2.imwrite(f"img_zoom_{x,y,s,sy}.png", img)

cam.set_zoom((0, 0, 1, 1))
img = cam.next_frame()
cv2.imwrite(f"img_zoom_full.png", img)
