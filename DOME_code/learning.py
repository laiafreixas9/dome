
import enum
#import tensorflow as tf
import logging


class Action(enum.Enum):
    """
    Action performed by the projector on the tracked volvox
    Either project light on it or do not
    """
    LIGHT_OFF = 0
    LIGHT_ON = 1

    def flip(self):
        if self is Action.LIGHT_ON:
            return Action.LIGHT_OFF
        else:
            return Action.LIGHT_ON


class Behaviour(enum.Enum):
    LEARN_STOP = 0
    LIGHT_ON_ALWAYS = 1
    LIGHT_OFF_ALWAYS = 2
    LEARN_DIRECTION = 3
    STOP_HARDCODED = 4
    STOP = 5  # This assumes the agent has learnt and prevents it from learning any more
    TRAIL = 6
    PATTERN = 7


def get_state_index(history, frames_on, frames_off, light_status):

    frames_on_value = min(10, frames_on)
    frames_off_value = min(10, frames_off)

    return frames_on_value + frames_off_value * 11 + light_status * 121


def get_state_reward(behaviour, velocity, acceleration, frames_on, frames_off):
    if behaviour == Behaviour.LEARN_STOP:
        if abs(velocity) < 0.05:
            return frames_off + frames_on
        else:
            if acceleration > 0:
                return -5
            return -1

    return 0


class State():
    def __init__(self):
        self.valid = False
        self.frames_on = 0
        self.frames_off = 0
        self.moving = False
        self.action = None  # will be of type Action


def create_network(state_shape, action_shape):
    learning_rate = 0.001
    model = tf.keras.Sequential()
    zero_initializer = tf.keras.initializers.Zeros()
    model.add(
        tf.keras.layers.Dense(
            24,
            input_shape=state_shape,
            activation='relu',
            kernel_initializer=zero_initializer))
    model.add(
        tf.keras.layers.Dense(
            12,
            activation='relu',
            kernel_initializer=zero_initializer))
    model.add(
        tf.keras.layers.Dense(
            action_shape,
            activation='linear',
            kernel_initializer=zero_initializer))
    model.compile(
        loss=tf.keras.losses.Huber(),
        optimizer=tf.keras.optimizers.Adam(
            lr=learning_rate),
        metrics=['accuracy'])
    return model
