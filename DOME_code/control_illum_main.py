# from picamera.array import PiRGBArray
# from picamera import PiCamera
# import RPi.GPIO as GPIO
import socket
import logging

from CameraController import CameraController
from Utils import write_to_file, Source


def run(img_source: Source, ip, port):
    cam = CameraController(img_source, ip, port)
    full_agent_list = cam.loop()

    write_to_file(full_agent_list, "full_agent_list.csv")


if __name__ == "__main__":

    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s [%(levelname)s] %(message)s",
        handlers=[
            logging.StreamHandler(),
            logging.FileHandler("illumination.log")
        ]
    )
    run(img_source=Source.EMULATED, ip='localhost', port=65455)
