import unittest
import Utils
import numpy as np
import cv2


class TestUtils(unittest.TestCase):
    def test_draw_circles(self):
        img = np.zeros((500, 500, 3))
        agent_list = []
        agent_list.append([100, 100, 10, 10, 90])
        Utils.draw_circles(img, agent_list)
        cv2.imshow("s", img)
        cv2.waitKey(0)


if __name__ == "__main__":
    c = TestUtils()
    c.test_draw_circles()
