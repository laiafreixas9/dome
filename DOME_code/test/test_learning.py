import unittest
import learning


class TestVolvoxLearning(unittest.TestCase):
    def test_init(self):
        m = learning.VolvoxLearningModel()
        self.assertEqual(m.behaviour(), learning.Behaviour.STOP)

    def test_predict_habituation(self):
        class Dummy:
            def __init__(self, action):
                self.action = action 

        def create_list( basic_list):
            output = []
            for val in basic_list:
                dummy = Dummy(learning.Action.LIGHT_ON)
                if val == 0 :
                    dummy.action = learning.Action.LIGHT_OFF
                output.append(dummy)

            return output
        m = learning.VolvoxLearningModel()

        sample_list = create_list([1,1,1,0,0,0])
        self.assertEqual(m.predicted_habituation_time(sample_list), [3,3])
        sample_list = create_list([0,0,0,1,1,1])
        self.assertEqual(m.predicted_habituation_time(sample_list), [3,3])
        sample_list = create_list([0,1,0,1,0,1,0,1,0,0,0,1,1,1])
        self.assertEqual(m.predicted_habituation_time(sample_list), [3,3])
        sample_list = create_list([0,1])
        self.assertEqual(m.predicted_habituation_time(sample_list), [1,1])
        sample_list = create_list([0,0,0,0,1,1,1,0,1])
        self.assertEqual(m.predicted_habituation_time(sample_list), [1,1])
        sample_list = create_list([1,0,0,0,1,1,1,0])
        self.assertEqual(m.predicted_habituation_time(sample_list), [1,3])
        sample_list = create_list([1,1,1,1,1,1,1,1])
        self.assertEqual(m.predicted_habituation_time(sample_list), [0,8])
        sample_list = create_list([0,0,0,0,0,0])
        self.assertEqual(m.predicted_habituation_time(sample_list), [6,0])
        
        