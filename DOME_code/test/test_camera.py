import unittest
import Camera


class TestCamera(unittest.TestCase):
    def test_init(self):
        c = Camera.Camera("matrix_parameters.txt")

    def test_load(self):
        c = Camera.Camera("matrix_parameters.txt")
        self.assertEqual(c.dx_cam, 222.898193359375)
        self.assertEqual(c.dy_cam, 185.50677490234375)
        self.assertEqual(c.theta, 0.01694755822893725)
        self.assertEqual(c.stretch_x, 0.39523429554896444)
        self.assertEqual(c.stretch_y, 0.41995929337440135)
        self.assertEqual(c.dx_proj, 453.0)
        self.assertEqual(c.dy_proj, 255.0)
